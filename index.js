/*
 * Copyright (c) AndyNgKM
 * Licensed under the MIT license
 * https://gitlab.com/andyngkokmam/tree-node-siblings/-/blob/main/LICENSE
 * @author AndyNgKM
 */
const app = require("./src/app")
const argv = require("yargs/yargs")(process.argv.slice(2))
    .usage("Usage: $0 <command> [options]")
    .example("$0 -f foo.js", "Search the siblings of tree in the given file")
    .alias("f", "file")
    .nargs("f", 1)
    .describe("f", "Load a file")
    .demandOption(["f"])
    .help("h")
    .alias("h", "help")
    .epilog("Copyright (c) AndyNgKM").argv

async function main(filePath) {
    const textOutput = await app.startApp(filePath)
    console.log("\n----- Output -----")
    console.log(Object.values(textOutput).join("\n"))
}

if (argv.file) {
    console.log("App - File selected : " + argv.file)
    main(argv.file)
}
