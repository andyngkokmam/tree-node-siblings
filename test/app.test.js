const describe = require("mocha").describe
const pre = require("mocha").before
const it = require("mocha").it
const assert = require("chai").assert
const expect = require("chai").expect
const sinon = require("sinon")

const app = require("../src/app")

const dataFilePath = "data/data_1.txt"
const dataInvalidFilePath = "data/data_invalid.txt"

describe("App test", () => {
    // Positive Cases
    it("should pass and return correct values", async () => {
        const textOutput = await app.startApp(dataFilePath)
        expect(textOutput).to.deep.equal({
            0: "Node 0 - No right node",
            1: "Node 1 - Right node is 2",
            2: "Node 2 - Right node is 3",
            3: "Node 3 - No right node",
            4: "Node 4 - Right node is 5",
            5: "Node 5 - Right node is 6",
            6: "Node 6 - No right node",
        })
    })

    // Negative Cases
    it("should throw an error if called without arg file path", async () => {
        try {
            await app.startApp()
        } catch (e) {
            assert.equal(e.message, "You must provide a file path.")
        }
    })

    // Exception Cases
    it("should throw an error if invalid data provided", async () => {
        try {
            await app.startApp(dataInvalidFilePath)
        } catch (e) {
            assert.equal(e.message, "Invalid Text")
        }
    })
})
