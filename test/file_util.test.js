const describe = require("mocha").describe
const pre = require("mocha").before
const it = require("mocha").it
const assert = require("chai").assert
const expect = require("chai").expect

const file_util = require("../src/file_util")

const dataFilePath = "data/data_2.txt"
const dataInvalidFilePath = "data/data_invalid.txt"

describe("Read text file", () => {
    const readTextFile = file_util.readTextFile
    // Positive Cases
    it("should pass and assign to array", async () => {
        const result = await readTextFile(dataFilePath)
        expect(result).to.deep.equal([
            [[-1, 0]],
            [
                [0, 1],
                [0, 2],
                [0, 3],
            ],
            [
                [1, 4],
                [1, 5],
                [3, 6],
            ],
            [
                [4, 7],
                [4, 8],
                [4, 9],
            ],
        ])
    })

    // Negative Cases
    it("should throw an error if called without arg file path", async () => {
        try {
            await readTextFile()
        } catch (e) {
            assert.equal(e.message, "You must provide a file path.")
        }
    })

    // Exception Cases
    it("should throw an error if text file data is invalid", async () => {
        let err = null
        try {
            await readTextFile(dataInvalidFilePath)
        } catch (e) {
            err = e
        }
        expect(err.message).to.be.equal("Invalid Text")
    })
})
