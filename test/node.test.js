const describe = require("mocha").describe
const pre = require("mocha").before
const it = require("mocha").it
const assert = require("chai").assert
const expect = require("chai").expect
const sinon = require("sinon")

const { Node } = require("../src/node")

describe("Class - Node", () => {
    const stubValue = {
        id: "1",
        children: [],
        right: null,
    }

    // Positive Cases
    it("should pass and create Node object", async () => {
        const node = new Node("1")
        expect(node.id).to.equal(stubValue.id)
        expect(node.children).to.deep.equal(stubValue.children)
        expect(node.right).to.equal(stubValue.right)
    })

    // Negative Cases
    it("should throw an error if create without an id", async () => {
        expect(function () {
            new Node().id
        }).to.throw(Error)
    })

    // Exception Cases
})
