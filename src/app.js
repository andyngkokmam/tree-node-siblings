/*
 * Copyright (c) AndyNgKM
 * Licensed under the MIT license
 * https://gitlab.com/andyngkokmam/tree-node-siblings/-/blob/main/LICENSE
 * @author AndyNgKM
 */
const file_util = require("./file_util")
const { Node } = require("./node")

const startApp = async (filePath) => {
    if (filePath === undefined || typeof filePath !== "string") {
        throw new Error("You must provide a file path.")
    }
    let firstLine = true
    let rootNode = null
    const lineDataList = await file_util.readTextFile(filePath)
    for (const lineData of lineDataList) {
        let currentNode = null
        if (firstLine) {
            currentNode = new Node(lineData[0][1])
            rootNode = currentNode
            firstLine = false
        } else {
            const noOfChild = lineData.length
            for (let i = 0; i < noOfChild; i++) {
                currentNode = new Node(lineData[i][1])
                if (i + 1 < noOfChild) {
                    currentNode.right = lineData[i + 1][1]
                }
                const parentNode = rootNode.getNodeById(lineData[i][0])
                parentNode.children[parentNode.children.length] = currentNode
            }
        }
    }
    return Node.displayChild(rootNode)
}
exports.startApp = startApp