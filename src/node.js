/*
 * Copyright (c) AndyNgKM
 * Licensed under the MIT license
 * https://gitlab.com/andyngkokmam/tree-node-siblings/-/blob/main/LICENSE
 * @author AndyNgKM
 */

class Node {
    constructor(id) {
        if (typeof id === "undefined") {
            throw new Error("Missing id")
        }
        this.id = id
        this.children = []
        this.left = null
        this.right = null
    }

    getChildNode(children, id) {
        let result = null
        for (const c of children) {
            if (c) {
                if (c.id === id) {
                    result = c
                    break
                } else if (c.children) {
                    result = this.getChildNode(c.children, id)
                    if (result !== null) {
                        break
                    }
                }
            }
        }
        return result
    }

    getNodeById(id) {
        if (this.id === id) {
            return this
        }
        if (this.children) {
            return this.getChildNode(this.children, id)
        }
    }

    static displayChild = (node) => {
        let displayText = {}
        if (node.right) {
            displayText[node.id] = `Node ${node.id} - Right node is ${node.right}`
        } else if (node.right === null) {
            displayText[node.id] = `Node ${node.id} - No right node`
        }
        const children = node.children
        if (children && children.length > 0) {
            for (const c of children) {
                if (c.right !== null) {
                    displayText[c.id] = `Node ${c.id} - Right node is ${c.right}`
                } else {
                    displayText[c.id] = `Node ${c.id} - No right node`
                }
            }
            for (const c of children) {
                if (c.children) {
                    const result = Node.displayChild(c)
                    if (result && Object.keys(result).length > 0) {
                        displayText = Object.assign(displayText, result)
                    }
                }
            }
        }
        return displayText
    }
}
exports.Node = Node
