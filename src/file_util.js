/*
 * Copyright (c) AndyNgKM
 * Licensed under the MIT license
 * https://gitlab.com/andyngkokmam/tree-node-siblings/-/blob/main/LICENSE
 * @author AndyNgKM
 */
const lineReader = require("line-reader"),
    Promise = require("bluebird")

function evalObj(obj) {
    try {
        eval("const arr=" + obj)
        return Function('"use strict";return (' + obj + ")")()
    } catch (err) {
        throw new Error("Invalid Text")
    }
}

const readTextFile = (filePath) => {
    return new Promise(async (resolve, reject) => {
        if (filePath === undefined || typeof filePath !== "string") {
            reject(new Error("You must provide a file path."))
        }
        const eachLine = Promise.promisify(lineReader.eachLine)
        try {
            let lineDataList = []
            await eachLine(filePath, function (line, last) {
                if (typeof line === "string") {
                    const lineStr = line.replace(/\s/g, "")
                    try {
                        const lineData = evalObj(lineStr)
                        lineDataList.push(lineData)
                    } catch (err) {
                        reject(err)
                    }
                }
            })
            resolve(lineDataList)
        } catch (err) {
            reject(err)
        }
    })
}
exports.readTextFile = readTextFile