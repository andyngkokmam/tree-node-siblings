# Tree-Node-Siblings

## Getting started - Install

### Prerequisites

- Install Node.js

Open a command prompt/powershell and run npm install to install the packages

```
npm install
```

## Usage

To start run the following command and specify a path to the data file. There are sample data files in data directory.

```
node index.js -f data/data_1.txt
```

## Build

Run build and the output will be stored to /dist directory

```
npm run build
```

## Test

Run test

```
npm run test
```
